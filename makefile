all:
	xelatex tc
	bibtex tc
	xelatex tc
	xelatex tc

clean:
	rm -f *.{aux,log,nav,out,snm,toc,vrb}
